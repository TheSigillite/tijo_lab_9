package pl.edu.pwsztar.domain.dto;

import java.util.List;

public class MovieDeleteDto {
    private List<Long> toDelete;

    public MovieDeleteDto(List<Long> toDelete) {
        this.toDelete = toDelete;
    }

    public MovieDeleteDto() {
    }

    public List<Long> getToDelete() {
        return toDelete;
    }
}
